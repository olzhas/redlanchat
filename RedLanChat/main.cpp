#pragma comment(lib, "ws2_32.lib")

#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <richedit.h>

#include "interface.h"
#include "net.h"
#include "resource.h"

wchar_t g_szClassName[] = TEXT("MainWindow");

HANDLE hUDPServer, hReDrawListBox;
DWORD dwUDPServer, dwReDrawListBox;
extern HWND hWnd;

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK AboutDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HINSTANCE hRichEditDLL = LoadLibrary(TEXT("riched20"));
	MSG msg;
	WNDCLASSEX wcMain;
	wcMain.cbClsExtra = 0;
	wcMain.cbSize = sizeof(WNDCLASSEX);
	wcMain.cbWndExtra = 0;
	wcMain.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wcMain.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcMain.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wcMain.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wcMain.hInstance = hInstance;
	wcMain.lpfnWndProc = WndProc;
	wcMain.lpszClassName = g_szClassName;
	wcMain.lpszMenuName = MAKEINTRESOURCE(IDM_MENU);
	wcMain.style = 0;

	if(!RegisterClassEx(&wcMain))
	{
		MessageBox(NULL, TEXT("Error: Can't register window class."), TEXT("Error"),
			MB_OK | MB_ICONERROR);
		return 0;
	}

	hWnd = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		g_szClassName,
		TEXT("RedLanChat"),
		WS_POPUPWINDOW | WS_CAPTION,
		CW_USEDEFAULT, CW_USEDEFAULT,
		640, 480,
		NULL, NULL, hInstance, NULL);

	if(hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error: Can't create window."), TEXT("Error"),
			MB_OK | MB_ICONERROR);
		return 0;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	while(GetMessage(&msg, NULL, NULL, NULL) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	WSACleanup();
	FreeLibrary(hRichEditDLL);

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		case WM_CREATE:
		{
			InitInterFace(hWnd);
			InitNet();

			hUDPServer = CreateThread(NULL, 0, UDPServer, 0, 0, &dwUDPServer);
			UDPRegisterUser();
			hReDrawListBox = CreateThread(NULL, 0, ReDrawListBox, 0, 0, &dwReDrawListBox);
		}
		break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_FILE_EXIT:
					PostMessage(hWnd, WM_CLOSE, 0, 0);
				break;

				case ID_HELP_ABOUT:
					DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_ABOUT), hWnd, AboutDlgProc);
				break;
			}
		break;
		case WM_NOTIFY:
		{	
			MSGFILTER *lpMsgFilter = (MSGFILTER*)lParam;
			switch(lpMsgFilter->msg)
			{
				case WM_KEYDOWN:
					if(lpMsgFilter->wParam == '\r')
					{
						NMHDR nmhdr = (NMHDR)lpMsgFilter->nmhdr;
						wchar_t buf2[512]={0};
						GetWindowTextW((HWND)nmhdr.hwndFrom, buf2, 1024);

						int size = GetWindowTextLength((HWND)nmhdr.hwndFrom);
						if(size > 0)
							UDPSend(buf2);

						SendMessage(nmhdr.hwndFrom, WM_SETTEXT, 0,0);
					}
				break;
			}
		}
		break;
		case WM_CLOSE:
			SendQuitMsg();
			DestroyWindow(hWnd);
		break;
		case WM_DESTROY:
			PostQuitMessage(0);
		break;
		default:
			return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	return 0;
}

BOOL CALLBACK AboutDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		case WM_INITDIALOG:
			return TRUE;
		break;
		case WM_DESTROY:
			EndDialog(hWnd, 0);
		break;
		case WM_CLOSE:
			PostMessage(hWnd, WM_DESTROY, 0, 0);
		break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDOK:
					PostMessage(hWnd, WM_CLOSE, 0, 0);
				break;
			}
		break;
		default:
			return FALSE;
	}
	return TRUE;
}
