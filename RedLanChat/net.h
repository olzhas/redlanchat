#ifndef NET_H
#define NET_H

#include <windows.h>

struct users{
	char nickname[100][16];
	struct in_addr ip[100];
	DWORD checkTime[100];
	int size;
};

void InitNet();
DWORD WINAPI UDPServer(LPVOID lpParam);
BOOL UDPSend(const wchar_t *string);
BOOL UDPRegisterUser();
DWORD ParseRecvData(const char* recvData, struct sockaddr_in clientaddr);
BOOL SendSuccessMsg(struct in_addr ip);
DWORD DoPing(struct in_addr ip);
DWORD ClearUsers();
void GetOwnIP(struct in_addr &ip);
BOOL SendQuitMsg();

#endif