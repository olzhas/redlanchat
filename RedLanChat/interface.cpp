/*
*	This is Free Software
*	And published under GNU GPL
*
*	copyright by RedLiner 2007
*	redliner.kz@gmail.com
*
*/
#include <windows.h>
#include <richedit.h>

#include "interface.h"
#include "net.h"
#include "func.h"


HWND hWnd;
struct users OnlineUsers;

void InitInterFace(HWND &hWnd)
{
	CHARFORMAT2 cfmEditChatBox;
	cfmEditChatBox.cbSize = sizeof(CHARFORMAT2);
	cfmEditChatBox.dwMask = CFM_FACE;
	wcscpy(cfmEditChatBox.szFaceName, TEXT("Courier New"));

	hEditChatBox = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		TEXT("RICHEDIT20W"), TEXT(""),
		WS_CHILD | WS_VISIBLE |WS_VSCROLL | ES_READONLY | ES_MULTILINE | ES_DISABLENOSCROLL,
		0, 0, 510, 336, hWnd,
		(HMENU)IDC_CHATBOX, GetModuleHandle(NULL), NULL);

	if(hEditChatBox == NULL)
	{
		MessageBox(NULL, TEXT("Cant create Chatbox"), NULL, MB_OK | MB_ICONERROR);
		return;
	}
	//COLORREF color = RGB(0x33, 0x66, 0x99);
	//SendMessage(hEditChatBox, EM_SETBKGNDCOLOR,NULL, (LPARAM)color);

	SendMessage(hEditChatBox, EM_SETTEXTMODE, (WPARAM)TM_MULTICODEPAGE, 0);
	SendMessage(hEditChatBox, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&cfmEditChatBox);

	hEditEntryBox = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		TEXT("RICHEDIT20W"),TEXT(""),
		WS_CHILD | WS_VISIBLE/* | ES_MULTILINE | WS_VSCROLL | ES_AUTOVSCROLL | ES_WANTRETURN*/,
		0, 340, 510, 60, hWnd,
		(HMENU)IDC_ENTRYBOX, GetModuleHandle(NULL),NULL);

	if(hEditEntryBox == NULL)
	{
		MessageBox(NULL, TEXT("Cant create entrybox"), NULL, MB_OK | MB_ICONERROR);
		return;
	}

	SendMessage(hEditEntryBox, EM_SETEVENTMASK, 0, (LPARAM)ENM_KEYEVENTS);
	SendMessage(hEditEntryBox, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&cfmEditChatBox);
	SendMessage(hEditEntryBox, EM_EXLIMITTEXT, 0, 500);
	//SendMessage(hEditEntryBox, EM_SETWORDWRAPMODE, WBF_WORDWRAP, 0);
	
	hListBoxUsers = CreateWindowEx(
		0,
		TEXT("LISTBOX"), TEXT(""),
		WS_VISIBLE | WS_CHILD | WS_VSCROLL,
		510, 0,
		120, 350,
		hWnd, NULL, GetModuleHandle(NULL), NULL);

	if(hListBoxUsers == NULL)
		MessageBox(NULL, TEXT("Cant create listbox"), NULL, MB_OK | MB_ICONERROR);

	/*
	NOTIFYICONDATA ntd;
	ntd.cbSize = sizeof(NOTIFYICONDATA);
	ntd.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	ntd.hWnd = hWnd;
	wcscpy(ntd.szTip, TEXT("RedLanChat"));
	ntd.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP;
	ntd.uID = 1;
	//DWORD msg;
	Shell_NotifyIcon(NULL, &ntd);
	//*/
	
}

void AddChatBoxText(const wchar_t *string)
{
    wchar_t	szNewChatBox[2048]={0},
			szChatBoxBuffer[2048]={0};

	char szBuf[2048]={0};
    int strLen = wcslen(string),
        bufLen;
	GetWindowTextW(hEditChatBox, szChatBoxBuffer, 2048);

    wcscat(szNewChatBox, szChatBoxBuffer);
    wcscat(szNewChatBox, string);

	SetWindowTextW(hEditChatBox, szNewChatBox); 
	SendMessage(hEditChatBox, EM_SCROLL, (WPARAM)SB_PAGEDOWN, 0);
}

DWORD WINAPI ReDrawListBox(LPVOID lpParam)
{
	int i, j=0;
	while(true)
	{
		if(hListBoxUsers != NULL)
		{
			for(i=1; i<OnlineUsers.size; i++)
			{
				if(GetTickCount() - OnlineUsers.checkTime[i] > 60 * 1000)
				{
					DoPing(OnlineUsers.ip[i]);
				}
			}
			if(ClearUsers() > 0 || j == 0)
			{
				wchar_t szNick[16]={0};
				
				SendMessage(hListBoxUsers, LB_RESETCONTENT, 0, 0);
				for(i=0;i<OnlineUsers.size;i++)
				{
					MultiByteToWideChar(CP_ACP, 0, OnlineUsers.nickname[i], 16, szNick, 16);
					SendMessage(hListBoxUsers, LB_ADDSTRING, 0, (LPARAM)szNick);
					SendMessage(hListBoxUsers, LB_SETITEMDATA, i, (LPARAM)i);
				}
				j=1;
			}
			Sleep(1 * 1000);
		}
	}
	return 0;
}
