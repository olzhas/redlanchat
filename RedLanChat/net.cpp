#include "net.h"
#include "interface.h"
#include "func.h"
#include <stdio.h>
#include <string.h>

HWND hEditChatBox;
HWND hEditEntryBox;
HWND hListBoxUsers;

extern struct users OnlineUsers;

void InitNet()
{
	int err;
	WSADATA wsaData;

	err = WSAStartup(MAKEWORD(2,2), &wsaData);
	if(err != 0)
	{
		MessageBox(NULL, TEXT("Error: Can't Init Winsock2 lib :("), NULL,
			MB_OK | MB_ICONERROR);
		return;
	}
}

DWORD WINAPI UDPServer(LPVOID lpParam)
{
	SOCKET sServer;
	sServer = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sServer == INVALID_SOCKET)
	{
		MessageBox(NULL, TEXT("Can't Create sServer SOCKET"), NULL,
			MB_OK | MB_ICONERROR);
		return 1;
	}

	struct sockaddr_in localaddr, clientaddr;

	localaddr.sin_addr.s_addr	= htonl(INADDR_ANY);
	localaddr.sin_family		= AF_INET;
	localaddr.sin_port			= htons(6060);

	if(bind(sServer, (sockaddr*)&localaddr, sizeof(localaddr)) != NULL)
	{
		MessageBox(NULL, TEXT("Cant bind 6060 port"), NULL,
			MB_OK | MB_ICONERROR);
		return 2;
	}

	char szRecvBuf[1024]={0},
		szCommandBuf[1024]={0},
		szNick[16]={0};
	struct in_addr MyIP;
	int nClientaddrSize;
	getNickName(szNick);
	GetOwnIP(MyIP);

	OnlineUsers.checkTime[0] = 0;
	strcpy(OnlineUsers.nickname[0], szNick);
	OnlineUsers.ip[0].s_addr = MyIP.s_addr;
	OnlineUsers.size = 1;

	while(true)
	{
		nClientaddrSize = sizeof(clientaddr);

		recvfrom(sServer, szRecvBuf, 1024, 0, (sockaddr*)&clientaddr, &nClientaddrSize);

		ParseRecvData(szRecvBuf, clientaddr);

		//AddChatBoxText(szRecvBuf);
		strcpy(szRecvBuf, "");

	}
	closesocket(sServer);

	return 0;
}

BOOL UDPSend(const wchar_t *string)
{
	SOCKET sClient;
	sClient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sClient == INVALID_SOCKET)
	{
		MessageBox(NULL, TEXT("error cant create socket UDPSend"), NULL,
			MB_OK | MB_ICONERROR);
		return FALSE;
	}

	struct sockaddr_in serveraddr;
	serveraddr.sin_port = htons(6060);
	serveraddr.sin_family = AF_INET;
	
	int i=0, ret=0, buflen=0;
	wchar_t buf[1024]={0};
	
	wcscpy(buf, (wchar_t*)"MSG\r\n");
	wcscat(buf, string);
	wcscat(buf, (wchar_t*)"\r\n");
	for(i=0; i<OnlineUsers.size; i++)
	{
		serveraddr.sin_addr.s_addr = OnlineUsers.ip[i].s_addr;
		ret = sendto(sClient, (char*)buf, 1024, 0, (sockaddr*)&serveraddr, sizeof(serveraddr));
		if(ret == SOCKET_ERROR)
		{
			MessageBox(NULL, TEXT("Cant send info"), NULL, MB_OK | MB_ICONERROR);
			closesocket(sClient);
			return 0;
		}
	}
	closesocket(sClient);
	return TRUE;
}

BOOL UDPRegisterUser()
{
	char szNickName[16]={0};// = "RedLiner";
	getNickName(szNickName);
	char szPacket[1024] = "NEW\r\nNICK ";
	strcat(szPacket, szNickName);
	strcat(szPacket, "\r\n");

	SOCKET sClient;
	sClient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if(sClient == INVALID_SOCKET)
	{
		MessageBox(NULL, TEXT("Cant Create Socket"), NULL, MB_OK | MB_ICONERROR);
		return FALSE;
	}

	BOOL bOptVal = TRUE;
	int nSizeVal = sizeof(BOOL);
	if(setsockopt(sClient, SOL_SOCKET, SO_BROADCAST, (char*)&bOptVal, nSizeVal) != 0)
	{
		MessageBox(NULL, TEXT("Cant set SO_BROADCAST option"), NULL, MB_OK | MB_ICONERROR);
		return FALSE;
	}

	struct sockaddr_in broadcastaddr;
	broadcastaddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	broadcastaddr.sin_family = AF_INET;
	broadcastaddr.sin_port = htons(6060);

	sendto(sClient, szPacket, sizeof(szPacket), 0, (sockaddr*)&broadcastaddr, sizeof(broadcastaddr));

	closesocket(sClient);

	return TRUE;
}

DWORD ParseRecvData(const char *recvData, struct sockaddr_in clientaddr)
{
	char szCommandBuf[1024]={0};
	getline(szCommandBuf, recvData);

	if(strcmp(szCommandBuf, "NEW") == 0)
	{
		substr(szCommandBuf, recvData, 5, 4);
		struct in_addr ip;
		GetOwnIP(ip);
		if(strcmp(szCommandBuf, "NICK") == 0 && (memcmp(&clientaddr.sin_addr, &ip, sizeof(in_addr)) != 0))
		{
			char szNickName[16];
			int nNickLen;
			substr(szNickName, recvData, 10, 16);
			nNickLen = strlen(szNickName);
			szNickName[nNickLen - 2] = '\0';
			strcpy(OnlineUsers.nickname[OnlineUsers.size], szNickName);
			OnlineUsers.checkTime[OnlineUsers.size] = GetTickCount();
			OnlineUsers.ip[OnlineUsers.size].s_addr = clientaddr.sin_addr.s_addr;
			SendSuccessMsg(OnlineUsers.ip[OnlineUsers.size]);
			OnlineUsers.size++;
		}
	}
	if(strcmp(szCommandBuf, "SUCCESS") == 0)
	{
		substr(szCommandBuf, recvData, 9, 4);
		if(strcmp(szCommandBuf, "NICK") == 0)
		{
			char szNickName[16];
			int nNickLen;
			substr(szNickName, recvData, 14, 16);
			nNickLen = strlen(szNickName);
			szNickName[nNickLen - 2] = '\0';
			strcpy(OnlineUsers.nickname[OnlineUsers.size], szNickName);
			OnlineUsers.checkTime[OnlineUsers.size] = GetTickCount();
			OnlineUsers.ip[OnlineUsers.size].s_addr = clientaddr.sin_addr.s_addr;
			OnlineUsers.size++;
		}
	}
	if(strcmp(szCommandBuf,"MSG") == 0)
	{
		wchar_t szRecvMsgBuf[1024]={0},
				szNickName[16]={0},
				szMsg[1024]={0};
		char	szBufMsg[1024]={0};//, szBufNick[16]={0};

		substr2(szBufMsg, recvData, 6, 1024-6);
		int i=0, j=0;
		for(i=0, j=0; j<1024; i++, j+=2)
		{
			if(szBufMsg[j] == '\r' && szBufMsg[j+1] == '\n')
			{
				szRecvMsgBuf[i] = MAKEWORD(szBufMsg[j], 0x00);
				szRecvMsgBuf[i+1] = MAKEWORD(szBufMsg[j+1], 0x00);
				i++;
				continue;
			}
			szRecvMsgBuf[i] = MAKEWORD(szBufMsg[j], szBufMsg[j+1]);
		}
		//substr((char*)szRecvMsgBuf, recvData, 5, 1024);
		//int i;

		for(i=0; i<OnlineUsers.size; i++)
		{
			if(OnlineUsers.ip[i].s_addr == clientaddr.sin_addr.s_addr)
			{
				MultiByteToWideChar(CP_ACP, 0, OnlineUsers.nickname[i], 16, szNickName, 16); 
				break;
			}
		}

		wsprintf(szMsg, TEXT("<%s> %s"), szNickName, szRecvMsgBuf);
		AddChatBoxText(szMsg);
	}
	if(strcmp(szCommandBuf, "PING") == 0)
	{
		char szPong[5] = "PONG";
		SOCKET sClient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if(sClient == INVALID_SOCKET)
		{
			MessageBox(NULL, TEXT("cant create sClient in ping testing"), NULL, NULL);
			return 3;
		}
		if(sendto(sClient, szPong, sizeof(szPong), 0, (sockaddr*)&clientaddr, sizeof(clientaddr)) == NULL)
		{
			MessageBox(NULL, TEXT("Error Cant Send Pong"), NULL, NULL);
			return 4;
		}
		closesocket(sClient);
	}
	if(strcmp(szCommandBuf, "PONG") == 0)
	{
		//DWORD PingTime = GetTickCount;
		int i, nI=0;
		for(i=1; i<OnlineUsers.size; i++)
		{
			if(OnlineUsers.ip[i].s_addr == clientaddr.sin_addr.s_addr)
			{
				nI = i;
				break;
			}
		}
		OnlineUsers.checkTime[nI] = GetTickCount();
	}
	if(strcmp(szCommandBuf, "QUIT") == 0)
	{
		int i,j;
		for(i=1, j=1;j<OnlineUsers.size;i++, j++)
		{
			if(OnlineUsers.ip[i].s_addr == clientaddr.sin_addr.s_addr)
			{
				j++;
			}
			OnlineUsers.ip[i].s_addr = OnlineUsers.ip[j].s_addr;
			strcpy(OnlineUsers.nickname[i], OnlineUsers.nickname[j]);
			OnlineUsers.checkTime[i] = OnlineUsers.checkTime[j];
		}
		OnlineUsers.size -= (j-i);
	}
	return 0;
}

DWORD DoPing(struct in_addr ip)
{
	SOCKET sClient;
	sClient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sClient == INVALID_SOCKET)
	{
		MessageBox(NULL, TEXT("Cant create socket DoPing func"), NULL, NULL);
		return 1;
	}
	struct sockaddr_in clientaddr;
	clientaddr.sin_addr.s_addr = ip.s_addr;
	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(6060);
	char szPing[5] = "PING";

	int ret = sendto(sClient, szPing, sizeof(szPing), 0, (sockaddr*)&clientaddr, sizeof(clientaddr));

	closesocket(sClient);

	return ret;
}

BOOL SendSuccessMsg(struct in_addr ip)
{
	SOCKET sClient;
	sClient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sClient == INVALID_SOCKET)
	{
		MessageBox(NULL, TEXT("Cant init socket SendSuccessMsg"), 0, 0);
		return FALSE;
	}

	struct sockaddr_in useraddr;
	useraddr.sin_addr.s_addr = ip.s_addr;
	useraddr.sin_family = AF_INET;
	useraddr.sin_port = htons(6060);

	char szSendBuf[1024] = "SUCCESS\r\nNICK ", buf[1024];
	getNickName(buf);
	strcat(szSendBuf, buf);
	//AddChatBoxText(szSendBuf);
	int ret = sendto(sClient, szSendBuf, sizeof(szSendBuf), 0, (sockaddr*)&useraddr, sizeof(useraddr));
	closesocket(sClient);
	return ret;
}

DWORD ClearUsers()
{
	int i, j=0;
	for(i=1, j=1; j < OnlineUsers.size; i++, j++)
	{
		while(GetTickCount() - OnlineUsers.checkTime[i] > 90 * 1000)
		{
			j++;
		}
		OnlineUsers.checkTime[i] = OnlineUsers.checkTime[j];
		strcpy(OnlineUsers.nickname[i], OnlineUsers.nickname[j]);
		OnlineUsers.ip[i].s_addr = OnlineUsers.ip[j].s_addr;
	}
	int prev = OnlineUsers.size;
	OnlineUsers.size -= (j - i);

	return ((prev - OnlineUsers.size)==0)?0:1;
}

void GetOwnIP(struct in_addr &ip)
{
	char szHostname[1024]={0};
	hostent *host;

	gethostname(szHostname, 1024);
	host = gethostbyname(szHostname);
	if(host != NULL)
		memcpy(&ip, host->h_addr_list[0], host->h_length);
}

BOOL SendQuitMsg()
{
	SOCKET sClient;
	sClient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sClient == INVALID_SOCKET)
	{
		MessageBox(NULL, TEXT("Cant create socket"), NULL, MB_OK | MB_ICONERROR);
		return FALSE;
	}
	struct sockaddr_in clientaddr;
	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(6060);
	int i=0;
	char szQuitMsg[1024]="QUIT";
	for(i=0;i<OnlineUsers.size;i++)
	{
		clientaddr.sin_addr.s_addr = OnlineUsers.ip[i].s_addr;
		sendto(sClient, szQuitMsg, 1024, 0, (sockaddr*)&clientaddr, sizeof(clientaddr));
	}
	return TRUE;
}
