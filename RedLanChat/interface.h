#ifndef INTERFACE_H
#define INTERFACE_H

#include <windows.h>

#define IDC_CHATBOX		2000	
#define IDC_ENTRYBOX	2001
#define IDC_USERLIST	2002

extern HWND hEditChatBox; // = NULL;
extern HWND hEditEntryBox; // = NULL;
extern HWND hListBoxUsers;

void InitInterFace(HWND &hWnd);
void AddChatBoxText(const wchar_t *string);
DWORD WINAPI ReDrawListBox(LPVOID lpParam);

#endif