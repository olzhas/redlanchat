#include "func.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

#define MAX_LENGTH 10000

char* substr(char *dest, const char* src, int from, int num)
{
	int i, nSize = num, sum = from + num;

	//dest = (char*)calloc(nSize, sizeof(char));

	for(i = from; i < sum && src[i] != '\0' ; i++)
	{
		dest[i - from] = src[i];
	}
	dest[i - from] = '\0';
	return dest;
}

char* substr2(char *dest, const char* src, int from, int num) /* not safe function =\ */
{
	int i, nSize = num, sum = from + num;

	//dest = (char*)calloc(nSize, sizeof(char));

	for(i = from; i < sum; i++)
	{
		dest[i - from] = src[i];
	}
	dest[i - from] = '\0';
	return dest;
}

void getNickName(char *dest)
{
	char szConfFileName[] = "main.conf";
	FILE *fConfig = fopen(szConfFileName, "a+");
	if(fConfig == NULL)
	{
		wchar_t szErrMsg[] = TEXT("Error cant read");
		strcat((char*)szErrMsg, szConfFileName);
		MessageBox(NULL, szErrMsg, NULL, MB_OK | MB_ICONERROR);
		return;
	}
	else {

		fscanf(fConfig, "%s", dest);
		if(strcmp(dest, "") == 0)
		{
			strcpy(dest, "Guest");
			fprintf(fConfig, "%s", dest);
		}
		fclose(fConfig);
	}
	//return dest;
}

char* getline(char *dest, const char *src)
{
	int i=0;
	while(i < MAX_LENGTH && src[i] != '\0' && (src[i] != '\r' && src[i+1] != '\n'))
	{
		dest[i] = src[i];		
		i++;
	}
	dest[i] = '\0';
	return dest;
}
int countLine(const char *src)
{
	int len = strlen(src),
		i, cnt=0;
	for(i=0; i<len && src[i] != '\0'; i++)
	{
		if(src[i] == '\n')
		{
			cnt++;
		}
	}
	return cnt;
}
