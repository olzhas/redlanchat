#ifndef FUNC_H
#define FUNC_H

char* substr(char *dest, const char* src, int from, int num);
char* substr2(char *dest, const char* src, int from, int num);
void getNickName(char *dest);
char* getline(char* dest, const char *src);
int countLine(const char *src);


//char szNickName[16];

#endif